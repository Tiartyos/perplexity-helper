# Perplexity Helper - Core Features (MVP)

## Overview
Simple web application to showcase Violentmonkey script for Perplexity named "Perplexity Helper".

## Core Features (MVP)

### Landing Page
- Add main layout with global state context for dark and light mode
- Add modal for video presentation
- Feature list

### Data Management for Dark and Light Mode
- LocalStorage using JSON

## Technical Notes
- Theme: Implement with shadcn/ui dark mode
